const STATUS_DISPLAY = document.querySelector('.notificacionjuego') // Poner qué jugador a ganado o si quedaron empatados
GAME_STATE = ["", "", "", "", "", "", "", "", ""], // Guarda el movimiento que haga el jugador (X 0 la 0)
WINNINGS = [ // posibilidades de que el jugador gane - posiciones
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
];

mensajeGanador = () => `El jugador ${jugador} ha ganado`;
mensajeEmpate = () => `El juego ha terminado en empate`,
turnoJugador = () => `Turno del jugador ${jugador}`;

let juegoActivo = true;
    jugador = "O";


 function main() {
   resultadoJuego(turnoJugador());
   listeners();
 }

  main()

 function listeners() {
  document.querySelector('.game-container').addEventListener('click', oprimirCelda);
  document.querySelector('.game-restart').addEventListener('click', borrarTablero);
 }

 function resultadoJuego (mensaje) {
  STATUS_DISPLAY.innerHTML = mensaje;
 }



function oprimirCelda(clickedCellEvent) {
  const clickedCell = clickedCellEvent.target;
  if (clickedCell.classList.contains('game-cell')) {
    const clickedCellIndex = Array.from(clickedCell.parentNode.children).indexOf(clickedCell);
      if(GAME_STATE[clickedCellIndex] !== '' || !juegoActivo) {
      return false;
      }


      celdaJudadorSelec(clickedCell,clickedCellIndex);
      validacionResultado();

  }
  console.log(clickedCell);

}

function borrarTablero(){
  juegoActivo = true;
  jugador = 'O';
  restablecer();
  resultadoJuego(turnoJugador());
  document.querySelectorAll('.game-cell').forEach(cell => cell.innerText = '');
}

function restablecer(){
  let i = GAME_STATE.length;
  while(i--) {
    GAME_STATE[i] = '';
  }
}


function celdaJudadorSelec(clickedCell, clickedCellIndex) {
  GAME_STATE[clickedCellIndex] = jugador;
  clickedCell.innerText = jugador; //
}

function validacionResultado() {
  ganadorRonda = false;
  for(let i = 0; i <WINNINGS.length; i++) {
      const condicionGanar = WINNINGS[i];
      let posicion1 = GAME_STATE[condicionGanar[0]];
          posicion2 = GAME_STATE[condicionGanar[1]];
          posicion3 = GAME_STATE[condicionGanar[2]];
      
      if (posicion1 === '' || posicion2 === '' || posicion3 === '') {
      continue;

      }
      if (posicion1 === posicion2 && posicion2 === posicion3) {
          ganadorRonda = true;
          break;

      }
  }

  if(ganadorRonda){
    resultadoJuego(mensajeGanador());
    juegoActivo = false;
    return;
  }

  let posicionVacia = !GAME_STATE.includes('');

  if(posicionVacia){
    resultadoJuego(mensajeEmpate());
    juegoActivo = false;
    return;

  }

  cambioDeJugador(); 
}

function cambioDeJugador(){
  jugador = (jugador === 'X')? 'O': 'X';
  resultadoJuego(turnoJugador());

}

